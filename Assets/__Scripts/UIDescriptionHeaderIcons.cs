using UnityEngine;

namespace __Scripts
{
    public class UIDescriptionHeaderIcons : MonoBehaviour
    {
        [SerializeField] private GameObject[] descriptionHeaderIcons;


        [ContextMenu("Hide all icons")]
        public void HideAllIcons()
        {
            foreach(var icon in descriptionHeaderIcons)
                icon.SetActive(false);
        }
    }
}
