using System;
using UnityEngine;

namespace __Scripts
{
    public class PositionBasedOnResolution : MonoBehaviour
    {
        public ResToPos[] resToPoses;
        public Transform target;
        
        
        [ContextMenu("CheckForResolutions")]
        public void CheckForResolutions()
        {
            var resolution = Screen.currentResolution;
            
            if(resToPoses == null) return;
            
            foreach (var res in resToPoses)
            {
                if (res.Height == resolution.height || res.Height == resolution.width)
                {
                    if (res.Width == resolution.width || res.Width == res.Height)
                    {
                        target.localPosition = res.Position;
                    }
                }
            }
        }
    }

    [Serializable]
    public struct ResToPos
    {
        public int Width;
        public int Height;
        public Vector3 Position;

        public ResToPos(int width, int height, Vector3 position)
        {
            Width = width;
            Height = height;
            Position = position;
        }
    }
}
