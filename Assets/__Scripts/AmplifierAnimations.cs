using System;
using UnityEngine;

namespace __Scripts
{
    public class AmplifierAnimations : MonoBehaviour
    {
        [SerializeField] private Transform parent;
        [SerializeField] private GameObject target;

        [ContextMenu("Up")]
        public void TurnUp()
        {
            //LeanTween.rotateAround(target, Vector3.up, 90, 1f).setEase(LeanTweenType.easeInOutQuad);
            LeanTween.rotateAroundLocal(target, parent.up, 90, 1f).setEase(LeanTweenType.easeInOutQuad);
        }
        
        [ContextMenu("Down")]
        public void  TurnDown()
        {
            //LeanTween.rotateAround(target, Vector3.up, 90, 1f).setEase(LeanTweenType.easeInOutQuad);
            LeanTween.rotateAroundLocal(target, parent.up, -90, 1f).setEase(LeanTweenType.easeInOutQuad);
            
        }


        [ContextMenu("Right")]
        public void TurnRight()
        {
            LeanTween.rotateAroundLocal(target,parent.right, -90, 1f).setEase(LeanTweenType.easeInOutQuad);
           
        }


        [ContextMenu("Left")]
        public void TurnLeft()
        {
            LeanTween.rotateAroundLocal(target,parent.right, 90, 1f).setEase(LeanTweenType.easeInOutQuad); 
        }


        [ContextMenu("Forward")]
        public void TurnForward()
        {
            LeanTween.rotateAroundLocal(target,parent.forward, 90, 1f).setEase(LeanTweenType.easeInOutQuad); 
        }
        
        [ContextMenu("Forward")]
        public void TurnBackward()
        {
            LeanTween.rotateAroundLocal(target,parent.forward, -90, 1f).setEase(LeanTweenType.easeInOutQuad); 
        }
        
    }
}
