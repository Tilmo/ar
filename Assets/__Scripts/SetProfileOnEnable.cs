using System;
using UnityEngine;

namespace __Scripts
{
    public class SetProfileOnEnable : MonoBehaviour
    {

        [SerializeField] private GameManager.Profile profile;

        private bool armed;
        private void OnEnable()
        { 
            if(!armed)
            {
                armed = true;
                return;
            }

            GameManager.Instance.SetProfile(profile);

#if UNITY_EDITOR
            Debug.Log("Profile set to .. " + profile.ToString() + "  " + (int)profile);
#endif
        }

        private void Start()
        {
            armed = true;
        }
    }
}
