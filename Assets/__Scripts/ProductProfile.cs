using System;
using UnityEngine;

namespace __Scripts
{
    [Serializable]
    public class ProductProfile
    {
        public GameManager.Profile profile;
        public string productName;
        public GameObject model;
       
        public GameObject[] catalogItems;

       


    }
}
