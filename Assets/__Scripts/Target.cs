using UnityEngine;

namespace __Scripts
{
    public class Target : MonoBehaviour
    {
        [SerializeField] private GameObject highlight;
        public CatalogButton catalogButton;

        public void SetButton(CatalogButton contender)
        {
            catalogButton = contender;
        }
        public void Highlight(bool highlightTarget)
        {
            highlight.SetActive(highlightTarget);
        }
    }
}
