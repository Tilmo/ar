using System;
using UnityEngine;
using UnityEngine.UI;

namespace __Scripts
{
    public class ImageColorChanger : MonoBehaviour
    {
        public Color color1;
        public Color color2;
        public Color color3;

        [SerializeField] private Image[] images;

        [ContextMenu("Change To Black")]
        public void ChangeToBlack()
        {
            if (ValidImagesCheck()) return;
            
            foreach (var image in images)
            {
                image.color = Color.black;
            }
        }
        

        [ContextMenu("Change To White")]
        public void ChangeToWhite()
        {
            if (ValidImagesCheck()) return;
            
            foreach (var image in images)
            {
                image.color = Color.white;
            }
        }

        private bool ValidImagesCheck()
        {
            if(images == null)
                Debug.Log("No images in images array !!");
            
            return images == null;
        }
        
    }
}
