using System;
using UnityEngine;

namespace __Scripts
{
    public class TargetTouch : MonoBehaviour
    {
        [SerializeField] private LayerMask layerMask;
        private void Update()
        {
            for (var i = 0; i < Input.touchCount; i++)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                
                if (Physics.Raycast(ray, out var raycastHit, layerMask))
                {
                    var x = raycastHit.collider.attachedRigidbody.gameObject.GetComponent<Target>().catalogButton;
                    
                    if(x==null) return;
                    
                    
                    GameManager.Instance.MakeSelection( x );
                }
            }

#if UNITY_EDITOR
            
            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log(Input.mousePosition);
                
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out var raycastHit, layerMask))
                {
                    Debug.Log(raycastHit.collider.attachedRigidbody.gameObject.name);
                    var x = raycastHit.collider.attachedRigidbody.gameObject.GetComponent<Target>().catalogButton;
                    
                    if(x==null) return;
                    
                    GameManager.Instance.MakeSelection(x);
                }
            }
#endif
         
        }
    }
}
