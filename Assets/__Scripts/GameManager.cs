using System;
using System.Collections;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace __Scripts
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        private CatalogButton selection = null;
        
        [SerializeField] private GameObject highLight;
        
        [SerializeField] private ProductProfile[] profiles;
        [SerializeField] private bool catalogInitialized;
        [SerializeField] private GameObject catalog;
        [SerializeField] private TMP_Text productNameField;

        [SerializeField] private PositionBasedOnResolution positionBasedOnResolution;


        public bool selectionEnabled;

        [SerializeField] private Profile profile;


        private void Start()
        {
            selectionEnabled = true;
            SetOffset();
        }

        public void SetProfile(Profile newProfile)
        {
            ChangeProfile(newProfile);
        }

        private void ChangeProfile(Profile candidate)
        {
            if(!selectionEnabled) return;
            
            if (profile == candidate)  return;

            if (candidate == Profile.None)
            {
                ResetCatalog();
            }
            else
            {
                if(!catalogInitialized)
                    catalog.SetActive(true);

                if (profile != Profile.None)
                {
                    RemoveProfileButtons(profile);
                    selection.Deactivate();
                }
                
                profile = candidate;
                var i = (int)candidate;

                var productProfile = profiles[i - 1];

                foreach (var button in productProfile.catalogItems)
                {
                    button.SetActive(true);
                }
                
                MakeSelection(productProfile.catalogItems[0].GetComponent<CatalogButton>());

                if (productProfile.model != null)
                {
                    productProfile.model.SetActive(true);
                    LeanTween.rotateAround(productProfile.model, Vector3.up, 360, time).setLoopClamp();
                }
                   

                productNameField.text = productProfile.productName;
                
                CatalogAutoResizer.Instance.AdjustHeight();
            }
        }

        private void RemoveProfileButtons(Profile target)
        {
            foreach (var productProfile in profiles)
            {
                if(productProfile.profile != target) continue;
                
                var buttons = productProfile.catalogItems;

                foreach (var button in buttons)
                {
                    button.SetActive(false);
                }

                if (productProfile.model != null)
                {
                    productProfile.model.SetActive(false);
                    LeanTween.cancel(productProfile.model);
                }
                   
            }
        }


        private void SetOffset()
        {
            positionBasedOnResolution.CheckForResolutions();
        }

        private void ResetCatalog()
        {
            profile = Profile.None;

            foreach (var productProfile in profiles)
            {
                var buttons = productProfile.catalogItems;

                foreach (var button in buttons)
                {
                    button.SetActive(false);
                }


                
                
                if(productProfile.model != null)
                    productProfile.model.SetActive(false);
            }
            
            CatalogAutoResizer.Instance.AdjustHeight();
            
            selection.Deactivate();
            
            catalogInitialized = false;
            catalog.SetActive(false);
        }
        
        
        public enum Profile
        {
            None, BluesJunior4, Rumble15
        }

        private int tweenId;

        [SerializeField] private float vectorFactor;
        [SerializeField] private float time;

        private void Awake()
        {
            Instance = this;
        }

        public void MakeSelection(CatalogButton catalogButton)
        {
            if (selection == null)
            {
                selection = catalogButton;
                catalogButton.Activate();

                highLight.SetActive(true);
                HighlightSelection(selection.transform);
            }
            else
            {
                selection.Deactivate();
                catalogButton.Activate();
                
                selection = catalogButton;
                
                HighlightSelection(selection.transform);
            }
        }

        public void HighlightSelection(Transform selectionTransform)
        {
            highLight.transform.position = selectionTransform.position;
            highLight.transform.SetParent(selectionTransform.transform);
        }
        
        
    }
}
