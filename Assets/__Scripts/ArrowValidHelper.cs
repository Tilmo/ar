using UnityEngine;
using Image = UnityEngine.UI.Image;

namespace __Scripts
{
    public class ArrowValidHelper : MonoBehaviour
    {
        [SerializeField] private Color valid;
        [SerializeField] private Color invalid;
        [SerializeField] private Color collapsedColor;
        [SerializeField] private Image image;

        [SerializeField] private float threshold;

        public bool collapsed;


        public void SetCollapsed(bool isCollapsed)
        {
            collapsed = isCollapsed;
            image.color = collapsedColor;
        }


        public void SetActiveBasedOnValue(Vector2 value)
        {
        
            if(collapsed) return;
        
            if (value.y > threshold)
            {
                if(image.color == invalid) return;
            
                image.color = invalid;
            }
            else
            {
                if(image.color == valid) return;
                image.color = valid;
            }

        }
    
    }
}
