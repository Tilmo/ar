using System;
using TMPro;
using UnityEngine;

namespace __Scripts
{
    public class CatalogButton : MonoBehaviour
    {
        [SerializeField] private GameObject header;
        [SerializeField] private GameObject text;
        

        [SerializeField] private Target[] targets;
        

        private ImageColorChanger colorChanger;
        
        private void Awake()
        {
            colorChanger = GetComponent<ImageColorChanger>();

            if (targets.Length > 0 && targets != null)
            {
                foreach (var target in targets)
                {
                    target.SetButton(this);
                }
            }
      
        }

        public void Deactivate()
        {
            if(header != null) 
                header.SetActive(false);
            
            text.SetActive(false);
            
            colorChanger.ChangeToBlack();

            if (targets.Length <= 0 || targets == null) return;
            foreach (var target in targets)
            {
                target.Highlight(false);
            }
        }
        
        public void Activate()
        {
            if(header != null) 
                header.SetActive(true);
            text.SetActive(true);
            
            colorChanger.ChangeToWhite();

            if (targets.Length <= 0 || targets == null) return;
            foreach (var target in targets)
            {
                target.Highlight(true);
            }
        }

        [ContextMenu("Select")]
        public void Select()
        {
            GameManager.Instance.MakeSelection(this);
        }
    }
}
