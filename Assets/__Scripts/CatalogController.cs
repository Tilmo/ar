using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace __Scripts
{
    public class CatalogController : MonoBehaviour
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private float collapseHeight;
        private float startingHeight;
        private float startingWidth;
        

        [SerializeField] private float tweenTime;
        [SerializeField] private Mask catalogBackgroundMask;

        private void Awake()
        {
            if (rectTransform == null)
            {
                Debug.Log("No Catalog Transform Set");
                return;
            }

            var rect = rectTransform.rect;
            startingHeight = rect.height;
            startingWidth = rect.width;
        }


        [ContextMenu("Collapse Catalog")]
        private void CollapseCatalog()
        {
            rectTransform.sizeDelta = new Vector2(startingWidth, collapseHeight);
            
        }

        [ContextMenu("Collapse Catalog LeanTween")]
        public void CollapseCatalogLean()
        {
            StartCoroutine(CatalogCloseLean());
        }
        
        [ContextMenu("Original Catalog LeanTween")]
        public void OriginalCatalogLean()
        {
            StartCoroutine(CatalogExpandLean());
        }


        [SerializeField] private float anticipationTime;

        private IEnumerator CatalogExpandLean()
        {
            
            
            
            yield return new WaitForSecondsRealtime(.15f);
            
           
            
            var id = LeanTween.size(rectTransform, new Vector2(startingWidth, startingHeight), tweenTime).setEase(LeanTweenType.easeInQuad).id;
            
            
            while (LeanTween.isTweening(id))
            {
                yield return null;
            }
            
            catalogBackgroundMask.enabled = false;
        }



        private IEnumerator CatalogCloseLean()
        {
            
       
      
            yield return new WaitForSecondsRealtime(.15f);
            catalogBackgroundMask.enabled = true;

            var id = LeanTween.size(rectTransform, new Vector2(startingWidth, collapseHeight), tweenTime).setEase(LeanTweenType.easeInQuad).id;

            while (LeanTween.isTweening(id))
            {
                yield return null;
            }
        }
    }
}
