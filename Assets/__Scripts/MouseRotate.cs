using System;
using Unity.VisualScripting;
using UnityEngine;

namespace __Scripts
{
    public class MouseRotate : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed;
        [SerializeField] private Transform target;
        [SerializeField] private LayerMask layerMask;

        [SerializeField] private LayerMask targetMask;

        public bool engaged;
        
        private Touch touch;
        private void Update()
        {
            if (engaged)
            {
                if (touch.phase == TouchPhase.Moved)
                {
                    var rotationY = Quaternion.Euler(0f, -touch.deltaPosition.x * rotationSpeed, 0f);
                    var rotationX = Quaternion.Euler(-touch.deltaPosition.y * rotationSpeed, 0f, 0f);
                        
                    target.rotation *= rotationY;
                    target.rotation *= rotationX;
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    engaged = false;
                }
                
                return;
            }
            
            for (var i = 0; i < Input.touchCount; i++)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                
                if (Physics.Raycast(ray, out var raycastHit, layerMask))
                {
                    if (!Physics.Raycast(ray, targetMask))
                    {
                        engaged = true;
                        touch = Input.GetTouch(i);
                    }
                }
            }


            

            
            
            
#if UNITY_EDITOR



#endif
        }

    }
}
