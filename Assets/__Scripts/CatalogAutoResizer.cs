using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace __Scripts
{
    public class CatalogAutoResizer : MonoBehaviour
    {
        public float buttonHeight;

        private float startingHeight;

        public RectTransform rectTransform;

        [SerializeField] private float offset;

        public static CatalogAutoResizer Instance;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            startingHeight = rectTransform.rect.height;

            Instance = this;
            
            AdjustHeight();
        }

        [ContextMenu("Number of Children Test")]
        private void PrintNumberOfChildren()
        {
            Debug.Log(GetTopLevelActiveChildren(transform));
        }


        [ContextMenu("Adjust Height")]
        public void AdjustHeight()
        {
            rectTransform.sizeDelta = new Vector2(100, buttonHeight * GetTopLevelActiveChildren(transform) - offset );
        }

        [ContextMenu("Slack float")]
        private void CalculateExtraHeight()
        {
            var height = startingHeight - (buttonHeight * GetTopLevelActiveChildren(transform));
            
            Debug.Log(height);
        }
        
        
        public static Transform[] GetTopLevelChildren(Transform Parent)
        {
            Transform[] Children = new Transform[Parent.childCount];
            for (int ID = 0; ID < Parent.childCount; ID++)
            {
                
                Children[ID] = Parent.GetChild(ID);
            }
            return Children;
        }

        public static int GetTopLevelActiveChildren(Transform Parent)
        {
            var children = GetTopLevelChildren(Parent);

            int i = 0;

            foreach (var child in children)
            {
                if (child.gameObject.activeSelf)
                    i++;
            }

            return i;
        }
        
    }
}
